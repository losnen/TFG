# TFG

[![Libro en gh-pages](https://img.shields.io/badge/Book-gh--pages-brightgreen.svg?style=flat-square)](http://Losnen.github.io/TFG)
[![Libro en Gitbook](https://img.shields.io/badge/Book-Gitbook-brightgreen.svg?style=flat-square)](https://losnen.gitbooks.io/tfg/content/)

> Documentación del TFG

En este libro encontrarás:

* Documentación sobre Ruby On Rails
* [Tutorial de Michael Hartl](https://www.railstutorial.org/book/)
