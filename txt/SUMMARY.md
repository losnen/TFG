# Summary

* [Rails tutorial by Michael Hartl](cap1/README.md)
    * [Comandos](cap1/COMANDOS.md)
    * [Teoría](cap1/TEORIA.md)
* [Plataforma Github Classroom](cap2/README.md)
    * [Análisis](cap2/ANALISIS.md)
