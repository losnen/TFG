# Comandos usados

> Para generar una aplicación en rails

```shell
$ rails [version] new app_name
```

> Para correr el servidor de forma local


```shell
$ rails server
```

> Instalar las dependencias

```shell
$ bundle install
```
> Para crear un modelo User con los atributos name y email.

```shell
$ rails generate scaffold User name:string email:string
```

> Para actualizar la base de datos.

```shell
$ rails db:migrate
```

> Crea los controladores y las vistas de las static pages

```shell
$ rails generate controller static_pages home
```

> Crea el modelo para el usuario.

```shell
$ rails generate model User name:string email:string
```

> Crea las pruebas de integración para users_signup

```shell
$ rails generate integration_test users_signup
```
