# Capitulo 1: Rails tutorial by Michael Hartl

* [From zero to deploy](https://www.railstutorial.org/book/beginning)
* [Toy app](https://www.railstutorial.org/book/toy_app)
* [Mostly static pages](https://www.railstutorial.org/book/static_pages)
