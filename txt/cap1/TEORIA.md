# Conceptos del tutorial


## Ficheros y directorios generados por rails.

* ```app/```	Código de la app como models, views, controllers y helpers
* ```app/assets```	CSS, JS o imágenes de la app.
* ```bin/```	Ficheros binarios
* ```config/```	Configuración de la app
* ```db/```	Ficheros para la base de datos
* ```doc/```	Documentación de la app
* ```lib/```	Módulos de librerías
* ```lib/assets```	Librerías de CSS, JS o imágenes.
* ```log/```	Log de la aplicación
* ```public/```	Datos accesibles al público, como páginas de error.
* ```bin/rails```	Programa para generar código, abrir sesiones de la consola o correr servidores
* ```test/```	Test de la app
* ```tmp/```	Ficheros temporales
* ```vendor/``` Código de terceros como plugins y otras gemas.
* ```vendor/assets```	Código de terceros como CSS, JS o imágenes.
* ```README.md```	Descripción de la app
* ```Rakefile```	Automatización de tareas
* ```Gemfile```	Gemas requeridas para la app
* ```Gemfile.lock``` Una lista de gemas para garantizar que todas los ficheros utilizan la misma versión de gema.
* ```config.ru```	Fichero de configuración para Rack middleware
* ```.gitignore``` Ficheros ignorados por git

## Modelo-Vista-Controlador (MVC)

Modelo-vista-controlador (MVC) es un patrón de arquitectura de software, que separa los datos y la lógica de negocio de una aplicación de la interfaz de usuario y el módulo encargado de gestionar los eventos y las comunicaciones. Para ello MVC propone la construcción de tres componentes distintos que son el modelo, la vista y el controlador, es decir, por un lado define componentes para la representación de la información, y por otro lado para la interacción del usuario


## Scaffold

Un Scaffold (andamio) en Rails es un conjunto completo de modelo: migración de base de datos, controlador para manipularlo, vistas para manipular los datos y un conjunto de pruebas para cada uno de los anteriores.

Al generar el modelo user con el siguiente comando:

```$ rails generate scaffold User name:string email:string```

Obtendremos las siguientes vistas y sus controladores respectivos:

* ```/users```	Página para mostrar los usuarios
* ```/users/1```	Página para mostrar el usuario con id 1
* ```/users/new```	Añadir un usuario.
* ```/users/1/edit```	Editar el ususario 1

## Resource

``` resources :users```

La referencia anterior crea 7 rutas en la aplicación:

* GET ```/users``` users#index Página para mostrar los usuarios
* GET	```/users/new```	users#new	Formulario para crear usuario nuevo
* POST /users/new```	users#create Añadir un usuario.
* GET ```/users/1```	users#show Página para mostrar el usuario 1
* GET	```/users/1/edit```	users#edit	Editar el ususario 1
* PATCH/PUT	/users/1	users#update	Actualiza el usuario 1
* DELETE	/users/1	users#destroy	Elimina la foto 1

## Validates

```Ruby
class Person < ApplicationRecord
  validates :name, presence: true
end

Person.create(name: "John Doe").valid? # => true
Person.create(name: nil).valid? # => false
```

Como se puede ver, validates nos permite saber que nuestra instancia de Person no es válida sin el atributo name. La segunda instancia no se mantendrá en la base de datos.


[Documentación oficial de Ruby on Rails sobre validación](http://guides.rubyonrails.org/active_record_validations.html)

## Static pages

Las páginas estáticas son iguales para cada usuario, de forma que cambian muy poco o nada. Por supuesto, que sean estáticas no quiere decir que esas páginas no será capaces de hacer algunas cosas interesantes.

## User Validations


```Ruby
before_save { email.downcase! }
validates :name, presence: true, length: { maximum: 50 } # Valida el nombre
VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
validates :email, presence: true, length: { maximum: 255 }, # Valida el correo
                  format: { with: VALID_EMAIL_REGEX },
                  uniqueness: { case_sensitive: false }
has_secure_password # Hace que la contraseña sea segura
validates :password, presence: true, length: { minimum: 6 } # Valida la contraseña


```

## Routes

```Ruby
root 'static_pages#home' # Landing page /
get  '/help',    to: 'static_pages#help' # Páginas estáticas
get  '/about',   to: 'static_pages#about'
get  '/contact', to: 'static_pages#contact'
get  '/signup',  to: 'users#new'
resources :users # Enruta todas las rutas relacionadas con /user



```
