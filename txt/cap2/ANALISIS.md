# Github Classroom - Análisis de la plataforma

## Funcionalidades de la página

* Se puede crear un classroom a partir una organización de Github.
* Dentro de cada Classroom se puede editar algunas opciones:
    * Editar el nombre del Classroom.
    * Añadir usuarios como administrador invitandolo mediante su github ID o con un enlace de invitación.
    * Añadir alumnos mediantre su github ID se puede hacer de 3 formas distrintas: uno a uno, con una lista de id o mediante un fichero CSV.
* Se pueden crear tareas o asignaciones en cada classroom:
    * Puede ser una asignación grupal o individual.
    * La asignación puede iniciarse desde un repositorio vecío o de un repositorio con códigos.
    * Se puede añadir una fecha de entrega.
    * Todos estos parámetros se pueden editar una vez creado la asignación.
